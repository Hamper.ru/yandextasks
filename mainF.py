inFileName = "input.txt"
outFileName = "output.txt"
inFile = open(inFileName, 'r', encoding="utf-8")
j = set(inFile.readline())
s = inFile.readline()
inFile.close()


d = dict({})
for b in s:
    if b>='a' and b<='z':
        d[b]= d.get(b, 0)+1

res=0
for b in j:
    res+=d.get(b, 0)

outFile = open(outFileName, 'w', encoding="utf-8")
outFile.write(str(res))
outFile.close()