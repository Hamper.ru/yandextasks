inFileName = 'input.txt'
outFileName = "output.txt"
inFile = open(inFileName, 'r', encoding="utf-8")
read_data = inFile.read()
aList = [int(x) for x in read_data.split()]
inFile.close()
res = sum(aList)
outFile = open(outFileName, 'w', encoding="utf-8")
outFile.write(str(res))
outFile.close()